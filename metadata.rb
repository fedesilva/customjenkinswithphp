name             "caseinc-jenkins"
maintainer       "federico silva"
maintainer_email "f@pragmagica.com"
license          "All rights reserved"
description      "Provides case inc's specific configurations for jenkins"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.0"

depends "yum"
depends "apt"

depends "java"
depends "jenkins"
depends "nginx"
depends "git"
depends "ohai"
depends "monit"
depends "php"
depends "phpunit"
depends "composer"
depends "simple_iptables"
depends "openssl"
depends "postfix"

