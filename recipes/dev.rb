

include_recipe "git"
include_recipe "php"
include_recipe "phpunit"
include_recipe "composer"

php_pear_channel "pear.pdepend.org" do
  action :discover
end

php_pear_channel "pear.netpirates.net" do 
  action :discover
end

php_pear_channel "pear.phpmd.org" do 
  action :discover
end

php_pear_channel "components.ez.no" do 
  action :discover
end

phpqa = php_pear_channel "pear.phpqatools.org" do
    action :discover
end
php_pear "phpqatools" do
  channel phpqa.channel_name
  action :install
end

case node.platform
when "redhat", "centos", "fedora"
  package "vim-enhanced"
  package "ctags-etags"
  package "tmux"
when "debian", "ubuntu"
  package "vim-nox"
  package "exuberant-ctags"
  package "tmux"
end

