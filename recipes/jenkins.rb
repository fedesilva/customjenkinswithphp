

node.override[:java] = {
  :install_flavor   => "oracle",
  :jdk_version      => "7",
  :oracle => {
    :accept_oracle_download_terms => "true"  
  }
}

node.override[:jenkins][:server][:plugins] = %w[ 
  git github github-api multiple-scms git-client ghprb github-oauth
  dashboard-view analysis-core ant token-macro
  checkstyle dry
  cloverphp htmlpublisher jdepend plot pmd violations xunit
  mysql-auth
]

node.override[:jenkins][:server][:host] = "localhost"


include_recipe "java"
include_recipe "jenkins"



