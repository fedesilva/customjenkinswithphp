

node.override[:nginx][:default_site_enabled] = false


include_recipe "nginx"

template "/etc/nginx/sites-available/jenkins" do 
  source "nginx/jenkins.conf.erb" 
  mode "0764"
  user "root"
  group "root"
  notifies :restart, resources(:service => "nginx")
end


directory "/etc/nginx/ssl" do
  owner "root"
  group "root"
  mode 00644
  action :create
end

cookbook_file "/etc/nginx/ssl/jenkins.case-dev.com.cert" do
  owner "root"
  group "root"
  notifies :reload, resources(:service => "nginx")
end

cookbook_file "/etc/nginx/ssl/jenkins.case-dev.com.key" do
  owner "root"
  group "root"
  notifies :reload, resources(:service => "nginx")
end

nginx_site "jenkins" 
