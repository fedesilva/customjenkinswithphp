#
# Cookbook Name:: caseinc-jenkins
# Recipe:: default
#
# Copyright (C) 2013 federico silva
# 
# All rights reserved - Do Not Redistribute
#

case node.platform
when "redhat", "centos", "fedora"
  include_recipe "yum::epel"
when "debian", "ubuntu"
  include_recipe "apt"
end

include_recipe "caseinc-jenkins::firewall"
include_recipe "caseinc-jenkins::jenkins"
include_recipe "caseinc-jenkins::nginx"
include_recipe "caseinc-jenkins::monit"
include_recipe "caseinc-jenkins::dev" # php, tools, conveniencias.


