# Caseinc-Jenkins Cookbook

## Provee un sistema de integración continua customizado para case inc.


# Requirements

* Ruby (probado con 2.0)0a
* Rubygems
* Bundler (`gem install bundler`)
* Berkshelf (`gem install berkshelf`)

## Test o instalaciones de desarrollo

* Vagrant
* Virtualbox

# Usage

## Test 

En el directorio del proyecto ejecutar

      vagrant up

Esto va a levantar una vm con debian 7 (la primera vez baja el box) y jenkins instalado.
La vm esta en una red interna al host en la ip 33.33.33.10 con el jenkins escuchando via
nginx en el puerto 80. Si uno mapea en /etc/hosts `33.33.33.10 jenkins.caseinc` también hay
un virtualhost sirviendo ese nombre.

Para instalaciones de prueba que usen github webhooks hay que configurar un localtunnel y
apuntarle al puerto 8888 del anfitrión de la vm.

# Attributes

# Recipes

## Default Recipe

Aplicar el recipe homónimo genera un jenkins server con lo necesario para correr php builds.

# Author

Federico Silva (<fede.silva@gmail.com>)
